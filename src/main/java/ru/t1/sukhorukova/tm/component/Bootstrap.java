package ru.t1.sukhorukova.tm.component;

import ru.t1.sukhorukova.tm.api.ICommandController;
import ru.t1.sukhorukova.tm.api.ICommandRepository;
import ru.t1.sukhorukova.tm.api.ICommandService;
import ru.t1.sukhorukova.tm.constant.ArgumentConst;
import ru.t1.sukhorukova.tm.constant.CommandConst;
import ru.t1.sukhorukova.tm.controller.CommandController;
import ru.t1.sukhorukova.tm.repository.CommandRepository;
import ru.t1.sukhorukova.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);

    public void start(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(String command) {
        switch (command) {
            case ArgumentConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.CMD_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.CMD_INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.CMD_ARGUMENT:
                commandController.showArguments();
                break;
            case ArgumentConst.CMD_COMMAND:
                commandController.showCommands();
                break;
            default:
                commandController.showArgumentError();
                break;
        }
    }

    private void processCommand(String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case CommandConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.CMD_HELP:
                commandController.showHelp();
                break;
            case CommandConst.CMD_INFO:
                commandController.showInfo();
                break;
            case CommandConst.CMD_EXIT:
                exit();
                break;
            case CommandConst.CMD_ARGUMENT:
                commandController.showArguments();
                break;
            case CommandConst.CMD_COMMAND:
                commandController.showCommands();
                break;
            default:
                commandController.showCommandError();
                break;
        }
    }

     private void exit() {
        System.exit(0);
    }

}
